; Universidad Internacional de la Rioja (UNIR)
; ESIT
; Master Universitario en Inteligencia Artificial
; Planificación escenario mono-plátano-caja
; Pablo Salgado
; Bogotá
; 16 de febrero de 2020

(define (problem problema1) (:domain MonoPlatanoCaja)
(:objects
    caja - bloque
    mono - animal
    platano - fruta
    sitio_a sitio_b sitio_c - sitio
)

(:init
    (en_localizacion mono sitio_a piso)
    (en_localizacion platano sitio_b techo)
    (en_localizacion caja sitio_c piso)
)

(:goal (and
    (tiene mono platano)
))

)
