; Universidad Internacional de la Rioja (UNIR)
; ESIT
; Master Universitario en Inteligencia Artificial
; Planificación escenario mono-plátano-caja
; Pablo Salgado
; Bogotá
; 16 de febrero de 2020

; Definición del dominio para el clásico problema en el que un mono debe alcanzar
; un plátano que está colgado del techo y dispone de una caja, la cual puede
; desplazar a otra posición y subir a ella para lograr el objetivo.
; El escenario tiene tres localizaciones posibles loc1, loc2, loc3 e inicialmente
; se indica que que localización está el mono, el platano y la caja.
(define (domain MonoPlatanoCaja)

; Se requiere strips dado que se van a usar solo precondiciones positivas.
; Se requiere typing dado que se van a usar "objetos" para representar los
; objetos del mundo real.
(:requirements :typing :strips)

; Se define la clase ubicación para determinar el sitio donde se encuentra cada
; objeto.
; Se define la clase animal porque el mono es la única entidad que realiza
; acciones
; Se define la clase nivel para indicar si el mono está en el piso o arriba de
; de la caja.
; La clase objeto se usar para instanciar la caja y el plátano y ubicarlos en
; su sitio
(:types
    ubicacion objeto - object
    animal bloque fruta - objeto
    sitio nivel - ubicacion
)

(:constants 
    piso - nivel
    techo - nivel
)

(:predicates 
    ; Verdadero si un objeto (mono, plátano, caja) está en la localización dada:
    ; (sitio, nivel)
    (en_localizacion ?objeto - objeto ?sitio - sitio ?nivel - nivel)

    ; Verdadero si el mono tiene el plátano.
    (tiene ?animal -animal ?fruta - fruta)
)

; Esta acción permite al mono ir de un sitio a otro. Sólo la puede ejecutar
; estando en el piso.
(:action ir_a
    :parameters (?animal - animal ?origen - sitio ?destino - sitio)
    :precondition (and 
        (en_localizacion ?animal ?origen piso)
    )
    :effect (and
        (en_localizacion ?animal ?destino piso)
        (not (en_localizacion ?animal ?origen piso))
    )
)

; Esta acción permite al mono mover la caja de un sitio a otro. Solo la puede
; realizar estando en el piso.
(:action mover_caja
    :parameters (?animal - animal ?caja - bloque ?origen - sitio ?destino - sitio)
    :precondition (and
        (en_localizacion ?animal ?origen piso)
        (en_localizacion ?caja ?origen piso)
    )
    :effect (and 
        (en_localizacion ?animal ?destino piso)
        (not (en_localizacion ?animal ?origen piso))

        (en_localizacion ?caja ?destino piso)
        (not (en_localizacion ?caja ?origen piso))
    )
)

; Esta acción permite al mono subirse a la caja. Sólo la puede realizar estando
; en el piso.
(:action subir_a_caja
    :parameters (?animal - animal ?caja - bloque ?origen - sitio)
    :precondition (and
        (en_localizacion ?animal ?origen piso)
        (en_localizacion ?caja ?origen piso)
    )
    :effect (and 
        (en_localizacion ?animal ?origen techo)
        (not (en_localizacion ?animal ?origen piso))
    )
)

; Esta acción permite al mono obtener el plátano.
(:action obtener_platano
    :parameters (?animal - animal ?platano - fruta ?origen - sitio ?nivel - nivel)
    :precondition (and 
        (en_localizacion ?animal ?origen ?nivel)
        (en_localizacion ?platano ?origen ?nivel)
    )
    :effect (and
        (tiene ?animal ?platano)
    )
)


)